package homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Entities;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by x-inv on 13.10.2016.
 */

public abstract class ProtoEntity extends HashMap<String,Object> {



    public boolean noError = true;

    protected abstract String[] StrKeys();

    protected abstract String[] ArrKeys();

    protected void Fill(JSONObject source){


        for (String key : StrKeys()){
            if (source.has(key)) {
                try {
                    this.put(key,source.getString(key));
                } catch (JSONException e) {
                    Log.e(">>>","error parce house " + e.toString());
                    noError = false;
                }
            }
            else {
                this.put(key,"");
            }
        }

        for (String key : ArrKeys()){
            ArrayList<String> value = new ArrayList<>();
            if (source.has(key)) {
                try {
                    JSONArray jsonArray = source.getJSONArray(key);
                    for (int i = 0; i < jsonArray.length(); i++) value.add(jsonArray.getString(i));
                } catch (JSONException e) {
                    Log.e(">>>","error parce house " + e.toString());
                    noError = false;
                }
            }
            this.put(key,value);
        }
    }
}
