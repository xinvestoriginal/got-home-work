package homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Activitys;

import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Models.GotModel;

/**
 * Created by x-inv on 24.10.2016.
 */

public class CharacterPresenter {

    private GotModel gotModel;
    private CharactersActivity activity;

    public CharacterPresenter(CharactersActivity activity){
        this.activity = activity;
        this.gotModel = GotModel.GetInstance(activity.getApplicationContext());
        this.activity.FillMenu(GetHosesNames());
    }

    public String[] GetHosesNames(){
        //ArrayList<HouseEntity> houses = gotModel.GetHouses();
        //String[] res = new String[houses.size()];
        //for (int i = 0; i < res.length; i++) res[i] = houses.get(i).GetName();
        //return res;
        return GotModel.LOADED_HOUSE_NAMES;
    }
}
