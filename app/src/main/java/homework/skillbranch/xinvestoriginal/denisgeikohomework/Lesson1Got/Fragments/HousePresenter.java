package homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Fragments;

import java.util.ArrayList;

import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Entities.MemberEntityLite;
import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Models.GotModel;
import homework.skillbranch.xinvestoriginal.denisgeikohomework.R;

/**
 * Created by x-inv on 24.10.2016.
 */

public class HousePresenter {

    private HouseFragment fragment;
    private int position;
    private GotModel gotModel;

    public HousePresenter(HouseFragment fragment, int position) {
        this.fragment = fragment;
        this.position = position;
        this.gotModel = GotModel.GetInstance(fragment.getContext());
        ArrayList<MemberEntityLite> members = this.gotModel.GetMembers(this.position);
        this.fragment.FillList(GetIconFromHouseName(GetHouseName()), members);
    }

    public static int GetIconFromHouseName(String houseName) {
        String[] names = GotModel.LOADED_HOUSE_NAMES;
        if (names[0].equals(houseName)) return R.drawable.targaryen_icon;
        if (names[1].equals(houseName)) return R.drawable.lanister_icon;
        return R.drawable.stark_icon;
    }

    public String GetHouseName() {
        return GotModel.LOADED_HOUSE_NAMES[position];
    }


}
