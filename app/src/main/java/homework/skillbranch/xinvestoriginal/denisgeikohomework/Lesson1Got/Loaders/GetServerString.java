package homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Loaders;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by x-invest on 11.09.2014.
 */


public abstract class GetServerString extends AsyncTask<String, Object, Object> {


    protected static final int MAX_ERROR_COUNT = 2;
    private   static final int MAX_TIME_OUT = 5000;

    private String GetStringFromRequest(String requestStr) {

        try {
            URL url = new URL(requestStr);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(MAX_TIME_OUT);
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.connect();

            InputStream inputStream = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream));

            String responseString = "";
            String line;

            while ((line = rd.readLine()) != null) responseString += line;

            return responseString;
        } catch (MalformedURLException e) {
            //Log.e(">>> ", e.toString());
            return null;
        } catch (IOException e) {
            //Log.e(">>> ", e.toString());
            return null;
        }
    }

    public String GetString(String request) {
        int errorCount = 0;
        String response = null;
        while (response == null && errorCount < MAX_ERROR_COUNT) {
            response = GetStringFromRequest(request);
            errorCount++;
        }
        return response;
    }



}