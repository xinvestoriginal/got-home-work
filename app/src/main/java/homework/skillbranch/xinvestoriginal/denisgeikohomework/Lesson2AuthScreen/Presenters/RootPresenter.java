package homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson2AuthScreen.Presenters;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.Nullable;

import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Activitys.SplashActivity;
import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson2AuthScreen.Activityes.RootActivity;
import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson2AuthScreen.Models.AuthHelper;

/**
 * Created by x-inv on 22.10.2016.
 */

public class RootPresenter {

    private RootActivity mActivity;
    private AuthHelper mAuthHelper;

    public RootPresenter(RootActivity activity){
        mActivity = activity;
        mAuthHelper = AuthHelper.GetInstanse(activity.getApplicationContext());
        mAuthHelper.rootPresenter = this;
    }

    private static boolean EMailIsValid(String source) {
        if (source == null) return false;
        String[] parts = source.split("@");
        if (parts.length != 2) return false;
        String address = parts[0];
        String domens = parts[1];
        if (address.length() == 0 || domens.length() == 0) return false;
        String[] subdomens = domens.split("\\.");
        if (subdomens.length < 2) return false;
        return subdomens[subdomens.length - 1].length() >= 2;
    }

    void takeView(Activity activity){
    }

    void dropView(){

    }

    void initView(){

    }

    @Nullable
    Activity getView(){
        return mActivity;
    }

    public boolean clickOnLogin(){
       //проверяем имеются ли сохраненные данные логина и пароля если имеются true
        return mAuthHelper.isAuth();
    }

    public void clickOnFb(){

    }

    public void clickOnVk(){

    }

    public void clickOnTwitter(){

    }

    public void clickOnShowCatalog(){
        mActivity.startActivity(new Intent(mActivity, SplashActivity.class));
    }

    public boolean GetToken(String mail, String pass){
        if (!EMailIsValid(mail)) return false;
        mAuthHelper.GetToken(mail,pass);
        mActivity.showLoad();
        return true;
    }

    public void onTokenObtained(){
        mActivity.hideLoad();
        mActivity.HidePanel();
    }
}
