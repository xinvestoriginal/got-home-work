package homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Activitys;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import homework.skillbranch.xinvestoriginal.denisgeikohomework.R;

public class DetailActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String HOUSE_INDEX_KEY = "houseIndex";
    private static final String MEMBER_NAME_KEY = "memberName";
    private DetailPresenter detailPresenter;

    public static void Show(Activity context, String houseName, String memberId) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra(HOUSE_INDEX_KEY, houseName);
        intent.putExtra(MEMBER_NAME_KEY, memberId);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        String houseName = getIntent().getStringExtra(HOUSE_INDEX_KEY);
        String memberId = getIntent().getStringExtra(MEMBER_NAME_KEY);

        detailPresenter = new DetailPresenter(this, houseName, memberId);
        detailPresenter.InitView();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle("");
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return false;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onClick(View v) {
        detailPresenter.onClick(v);
    }
}
