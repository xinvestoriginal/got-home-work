package homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson2AuthScreen.Helpers;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

/**
 * Created by x-inv on 23.10.2016.
 */

public class TypefaceHelper {

    private static final String FONT_ASSET_PATH = "PTBebasNeueBook.ttf";

    public static void SetTypeface(Context context, TextView view){
        Typeface font = Typeface.createFromAsset(context.getAssets(), FONT_ASSET_PATH);
        view.setTypeface(font);
    }
}
