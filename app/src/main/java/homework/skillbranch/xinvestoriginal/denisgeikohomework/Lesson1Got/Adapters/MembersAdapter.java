package homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Entities.MemberEntityLite;
import homework.skillbranch.xinvestoriginal.denisgeikohomework.R;

/**
 * Created by x-inv on 13.10.2016.
 */

public class MembersAdapter extends BaseAdapter {

    private ArrayList<MemberEntityLite> members;
    private LayoutInflater inflater;
    private int iconID;

    public MembersAdapter(Context context, ArrayList<MemberEntityLite> members, int iconID) {
        this.members = members;
        this.iconID  = iconID;
        this.inflater  = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return members.size();
    }

    @Override
    public Object getItem(int position) {
        return members.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null){
            convertView = inflater.inflate(R.layout.house_unit_item,null);
        }
        MemberEntityLite entity = members.get(position);

        TextView tvName = (TextView)convertView.findViewById(R.id.tvName);
        tvName.setText(entity.name);

        TextView tvCulture = (TextView)convertView.findViewById(R.id.tvCulture);
        tvCulture.setText(entity.title);

        ImageView ivUnitIcon = (ImageView)convertView.findViewById(R.id.ivUnitIcon);
        ivUnitIcon.setImageResource(iconID);

        Log.e("!!!", String.valueOf(parent.getChildCount()));

        return convertView;
    }
}
