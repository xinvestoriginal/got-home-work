package homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson2AuthScreen.Models;

import android.os.Parcel;
import android.os.Parcelable;
import android.preference.Preference;

/**
 * Created by x-inv on 22.10.2016.
 */

public class AuthScreenState  extends Preference.BaseSavedState {

    public static final Parcelable.Creator<AuthScreenState> CREATOR =
            new Parcelable.Creator<AuthScreenState>() {
                public AuthScreenState createFromParcel(Parcel in) {
                    return new AuthScreenState(in);
                }

                @Override
                public AuthScreenState[] newArray(int size) {
                    return new AuthScreenState[0];
                }
            };



    public AuthScreenState(Parcelable superState) {
        super(superState);
    }

    private int state;

    public AuthScreenState(Parcel in) {
        super(in);
        state = in.readInt();
    }





    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeInt(state);
    }


}
