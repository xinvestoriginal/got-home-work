package homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Entities;


import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by x-inv on 13.10.2016.
 */

public class MemberEntity extends ProtoEntity {

    public static final String CULTURE_KEY = "culture";
    public static final String NAME_KEY    = "name";
    public static final String ALIASES_KEY = "aliases";
    public static final String DIED_KEY    = "died";
    public static final String SERIES_KEY  = "tvSeries";
    public static final String MOTHER_KEY  = "mother";
    public static final String FATHER_KEY  = "father";
    public static final String URL_KEY     = "url";
    public static final String TITLES_KEY = "titles";

    private static final String[] strKeys = {URL_KEY,NAME_KEY,"gender",CULTURE_KEY,
                                             "born",DIED_KEY,FATHER_KEY,MOTHER_KEY,"spouse" };


    private static final String[] strArrs = {TITLES_KEY, ALIASES_KEY, "allegiances",
                                             "books","povBooks",SERIES_KEY,"playedBy" };

    public MemberEntity(JSONObject source) {
        Fill(source);
    }

    public String DiedText(){
        String died = (String)this.get(DIED_KEY);
        if (died == null || died.length() == 0) return null;
        ArrayList<String> seasons = (ArrayList<String>) this.get(SERIES_KEY);
        if (seasons == null || seasons.size() == 0) return null;
        return "This person died in " + seasons.get(seasons.size() - 1);
    }

    @Override
    protected String[] StrKeys() {
        return strKeys;
    }

    @Override
    protected String[] ArrKeys() {
        return strArrs;
    }

    public String toArrString(String arrKey){
        ArrayList<String> aliases = (ArrayList<String>) this.get(arrKey);
        String res = "";
        for (String a : aliases){
            if (res.length()>0) res += "\n";
            res += a;
        }
        return res;
    }
}
