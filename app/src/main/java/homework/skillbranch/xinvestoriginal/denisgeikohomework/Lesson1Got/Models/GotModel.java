package homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Models;

import android.content.Context;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.Calendar;

import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Activitys.SplashPresenter;
import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Entities.HouseEntity;
import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Entities.MemberEntity;
import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Entities.MemberEntityLite;
import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Loaders.AllHousesLoader;
import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.SQLite.BaseHelper;

/**
 * Created by x-inv on 24.10.2016.
 */

public class GotModel {

    public static final String[] LOADED_HOUSE_NAMES = {"House Targaryen of King's Landing",
                                                        "House Lannister of Casterly Rock",
                                                        "House Stark of Winterfell"};
    private static int MIN_TIME_LOAD_MS = 3000;
    private static GotModel instance = null;
    private boolean isLoad;
    private SplashPresenter splashPresenter;
    public GotModel(final Context context) {
        this.isLoad  = false;
        this.splashPresenter = null;
        BaseHelper.Init(context);
    }

    public static synchronized GotModel GetInstance(Context context) {
        if (instance == null) instance = new GotModel(context);
        return instance;
    }

    @Nullable
    public HouseEntity GetHouse(int pos) {
        return GetHouse(LOADED_HOUSE_NAMES[pos]);
    }

    @Nullable
    public MemberEntity GetMember(String id) {
        return BaseHelper.GetInstance().GetMember(id);
    }

    @Nullable
    public HouseEntity GetHouse(String name) {
        return BaseHelper.GetInstance().LoadHouse(name);
    }


    public ArrayList<MemberEntityLite> GetMembers(int houseIndex) {
        return GetMembers(LOADED_HOUSE_NAMES[houseIndex]);
    }

    public ArrayList<MemberEntityLite> GetMembers(String house) {
        return BaseHelper.GetInstance().GetMembers(house);
    }

    public void SetSplashPresenter(SplashPresenter p){
        if (splashPresenter != null) splashPresenter.Destroy();
        splashPresenter = p;
    }

    @Nullable
    public String FindMemberIdFromUrl(String url) {
        return BaseHelper.GetInstance().GetMemberIdFromUrl(url);
    }

    public boolean reLoadHousesFromBase(){

        if (isLoad) return false;
        isLoad = true;
        new AllHousesLoader(){

            int fullMemberCount;
            int houseCount;

            @Override
            protected void onProgressUpdate(Object... values) {
                super.onProgressUpdate(values);
                final SplashPresenter sp = splashPresenter;
                if (sp != null){
                    int progress = Math.round(100 * ((Integer)values[0])/fullMemberCount);
                    sp.onLoadProgress(progress);
                }
            }

            @Override
            protected Object doInBackground(String... params) {

                long startTime = Calendar.getInstance().getTimeInMillis();
                houseCount = BaseHelper.GetInstance().GetHouseCount();

                if (houseCount == 0) {
                    ArrayList<HouseEntity> houses = GetAllHouses(LOADED_HOUSE_NAMES);
                    fullMemberCount = 0;
                    for (HouseEntity h : houses) {
                        fullMemberCount += h.GetMembersUrls().size();
                    }
                    int currentCount = 0;
                    for (HouseEntity h : houses) {
                        int count = GetMembers(h.GetName(), h.GetMembersUrls(), currentCount);
                        currentCount += count;
                    }

                    houseCount = houses.size();
                    houses = null;
                }

                long endTime = Calendar.getInstance().getTimeInMillis();
                long elapsedTime = endTime - startTime;
                if (elapsedTime < MIN_TIME_LOAD_MS){
                    try {
                        Thread.sleep(MIN_TIME_LOAD_MS - elapsedTime);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object result){
                super.onPostExecute(result);
                final SplashPresenter sp = splashPresenter;
                if (sp != null){
                    sp.onHousesLoad(houseCount > 0);
                }
                isLoad = false;
            }
        }.execute();
        return true;
    }

}
