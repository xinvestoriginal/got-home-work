package homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Activitys;

import android.widget.Toast;

import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Models.GotModel;
import homework.skillbranch.xinvestoriginal.denisgeikohomework.R;

/**
 * Created by x-inv on 24.10.2016.
 */

public class SplashPresenter {

    private SplashActivity activity;
    private GotModel       gotModel;

    public SplashPresenter(SplashActivity activity){
        this.activity = activity;
        this.gotModel = GotModel.GetInstance(activity.getApplicationContext());
        this.gotModel.SetSplashPresenter(this);
        this.gotModel.reLoadHousesFromBase();
    }

    public void onLoadProgress(int percent){
        activity.SetProgress(String.valueOf(percent)+"%");
    }

    public void onHousesLoad(boolean isOk){

        if (isOk){
            activity.StartCharactersListActivity();
        }else{
            Toast.makeText(activity, activity.getString(R.string.no_houses_text), Toast.LENGTH_SHORT).show();
        }
    }

    public void Destroy(){
        activity.finish();
    }
}
