package homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Activitys;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import homework.skillbranch.xinvestoriginal.denisgeikohomework.R;

public class SplashActivity extends AppCompatActivity {

    private TextView tvSetupProgress;
    private SplashPresenter splashPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tvSetupProgress = (TextView)findViewById(R.id.tvSetupProgress);
        splashPresenter = new SplashPresenter(this);
    }

    public void StartCharactersListActivity(){
        startActivity(new Intent(this,CharactersActivity.class));
        finish();
    }

    public void SetProgress(String progressStr){
        tvSetupProgress.setText(progressStr);
    }

}
