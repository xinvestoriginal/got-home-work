package homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Loaders;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Entities.HouseEntity;
import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Entities.MemberEntity;
import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.SQLite.BaseHelper;

/**
 * Created by x-inv on 12.10.2016.
 */

public abstract class AllHousesLoader extends GetServerString {

    private static final String NULL_HOUSE_RESPONSE = "[]";

    private String GetHosesReq(int page){
        return "http://www.anapioficeandfire.com/api/houses?page=" + String.valueOf(page);
    }

    private String GetOneHouseReq(String houseName){
        String req = "http://www.anapioficeandfire.com/api/houses?name=" + houseName;
        return req.replace(" ","%20");
    }

    private boolean ResponseNotEmpty(String resp){
        return resp != null && !NULL_HOUSE_RESPONSE.equals(resp);
    }

    private ArrayList<HouseEntity> GetHousesFromResponse( String resp){
        ArrayList<HouseEntity> res =  new ArrayList<>();
        try {
            JSONArray jsonArr = new JSONArray(resp);
            for (int i = 0; i < jsonArr.length(); i++){
                JSONObject houseJsonObject = jsonArr.getJSONObject(i);

                HouseEntity e = new HouseEntity(houseJsonObject);
                if (e.noError) {
                    BaseHelper.GetInstance().AddHouseAsString(e.GetName(), houseJsonObject.toString());
                    res.add(e);
                }
            }
        } catch (JSONException e) {
            Log.e(">>>","error parce page " + e.toString());
        }
        return res;
    }

    public ArrayList<HouseEntity> GetAllHouses(String[] houseNames){
        ArrayList<HouseEntity> res = new ArrayList<>();
        for (String name : houseNames){
            String req = GetOneHouseReq(name);
            String resp = GetString(req);
            res.addAll(GetHousesFromResponse(resp));
        }
        return res;
    }

    public ArrayList<HouseEntity> GetAllHouses(){
        String resp;
        ArrayList<HouseEntity> res = new ArrayList<>();
        int page = 1;
        do{
            String request = GetHosesReq(page);
            resp = GetString(request);

            if (resp != null){
                res.addAll(GetHousesFromResponse(resp));
            }
            page++;
        }while(ResponseNotEmpty(resp));
        return res;
    }

    public int GetMembers(String houseName, ArrayList<String> urls, int elapsedCount) {
        int count = 0;
        for (String url : urls){
            String resp = GetString(url);
            if (resp != null){
                try {
                    JSONObject memberJsonObject = new JSONObject(resp);
                    MemberEntity e = new MemberEntity(memberJsonObject);
                    if (e.noError) {
                        BaseHelper.GetInstance().AddMemberAsString(
                                houseName, memberJsonObject.toString(), url);
                    }
                    e = null;
                    count++;
                    publishProgress(elapsedCount + count);
                } catch (JSONException e) {
                    Log.e(">>>","error parce unit " + e.toString());
                }
            }
        }
        return count;
    }
}
