package homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Activitys;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Entities.HouseEntity;
import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Entities.MemberEntity;
import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Fragments.HousePresenter;
import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Models.GotModel;
import homework.skillbranch.xinvestoriginal.denisgeikohomework.R;

/**
 * Created by x-inv on 24.10.2016.
 */

public class DetailPresenter {

    private GotModel       gotModel;
    private DetailActivity activity;
    private HouseEntity       house;
    private MemberEntity     member;

    public DetailPresenter(DetailActivity activity, String houseName, String memberId) {
        this.activity = activity;
        this.gotModel = GotModel.GetInstance(activity.getApplicationContext());
        this.house = this.gotModel.GetHouse(houseName);
        this.member = this.gotModel.GetMember(memberId);
    }

    public static int GetBannerFromHouseName(String houseName){
        String[] names = GotModel.LOADED_HOUSE_NAMES;
        if (names[0].equals(houseName)) return R.drawable.targarien;
        if (names[1].equals(houseName)) return R.drawable.lannister;
        return R.drawable.starks;
    }

    public void InitView(){
        ImageView ivDetailsBanner = (ImageView)activity.findViewById(R.id.ivDetailsBanner);
        ivDetailsBanner.setImageResource(DetailPresenter.GetBannerFromHouseName(house.GetName()));

        LayoutInflater li = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout llDetailMainContainer = (LinearLayout)activity.findViewById(R.id.llDetailMainContainer);
        String title = "Words";
        String value = house.get(title.toLowerCase()).toString();
        llDetailMainContainer.addView(GetTableItem(li,false,title,value));


        title = "Name";
        value = member.get(title.toLowerCase()).toString();
        llDetailMainContainer.addView(GetTableItem(li,false,title,value));

        title = "Born";
        value = member.get(title.toLowerCase()).toString();
        llDetailMainContainer.addView(GetTableItem(li,false,title,value));

        title = "Titles";
        value = member.toArrString(title.toLowerCase());
        llDetailMainContainer.addView(GetTableItem(li,false,title,value));

        title = "Aliases";
        value = member.toArrString(title.toLowerCase());
        llDetailMainContainer.addView(GetTableItem(li,false,title,value));

        title = "Father";
        value = member.get(title.toLowerCase()).toString();
        final String fUrl = value;
        if (value.length() > 0){
            String id = this.gotModel.FindMemberIdFromUrl(value);
            MemberEntity e = id != null ? this.gotModel.GetMember(id) : null;
            value = e != null ? (String) e.get(MemberEntity.NAME_KEY) : " - ";
        }
        View fView = GetTableItem(li,true,title,value);
        fView.setOnClickListener(activity);
        fView.setTag(fUrl);
        llDetailMainContainer.addView(fView);

        title = "Mother";
        value = member.get(title.toLowerCase()).toString();
        final String mUrl = value;
        if (value.length() > 0){
            String id = this.gotModel.FindMemberIdFromUrl(value);
            MemberEntity e = id != null ? this.gotModel.GetMember(id) : null;
            value = e != null ? (String) e.get(MemberEntity.NAME_KEY) : " - ";
        }
        View mView = GetTableItem(li,true,title,value);
        mView.setOnClickListener(activity);
        mView.setTag(mUrl);
        llDetailMainContainer.addView(mView);


        String died = member.DiedText();
        if (died != null){
            Snackbar.make(llDetailMainContainer, died, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }
    }


    private View GetTableItem(LayoutInflater inflater,boolean isParentItem,String title,String value){
        View res = inflater.inflate(R.layout.detail_item,null);

        if (value.length() == 0) value = "-";

        ImageView ivDetailItemIcon = (ImageView)res.findViewById(R.id.ivDetailItemIcon);
        int visibleState = isParentItem ? View.INVISIBLE : View.VISIBLE;
        ivDetailItemIcon.setImageResource(HousePresenter.GetIconFromHouseName(house.GetName()));
        ivDetailItemIcon.setVisibility(visibleState);

        TextView tvDetailItemKey = (TextView)res.findViewById(R.id.tvDetailItemKey);
        tvDetailItemKey.setText(title + ":");

        TextView tvDetailItemValue = (TextView)res.findViewById(R.id.tvDetailItemValue);
        tvDetailItemValue.setText(value);
        if (isParentItem) {
            int color;
            if (value.length() > 1){
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    color = activity.getResources().getColor(R.color.colorAccent,null);
                }else{
                    color = activity.getResources().getColor(R.color.colorAccent);
                }
                tvDetailItemValue.setBackgroundColor(color);
                tvDetailItemValue.setTextColor(Color.WHITE);
            }

            tvDetailItemKey.setTypeface(null, Typeface.BOLD);
            tvDetailItemValue.setTypeface(null, Typeface.BOLD);
        }

        return res;
    }

    public void onClick(View v) {
        String url = (String) v.getTag();
        if (url != null && url.length() > 0){
            String id = this.gotModel.FindMemberIdFromUrl(url);
            if (id != null) {
                DetailActivity.Show(activity, house.GetName(), id);
                activity.finish();
            }
        }
    }
}
