package homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Fragments.HouseFragment;

/**
 * Created by x-inv on 12.10.2016.
 */

public class SectionsPagerAdapter extends FragmentPagerAdapter {

    private String[] houses;

    public SectionsPagerAdapter(FragmentManager fm, String[] houses) {
        super(fm);
        this.houses = houses;
    }

    @Override
    public Fragment getItem(int position) {
        return HouseFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return houses.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return this.houses[position];
    }
}
