package homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Entities;

import android.support.annotation.Nullable;

import java.util.ArrayList;

/**
 * Created by x-inv on 25.10.2016.
 */

public class MemberEntityLite {

    public String id;
    public String name;
    public String title;

    private MemberEntityLite(String id, String name, ArrayList<String> titles) {
        this.id = id;
        this.name = name;
        this.title = "";
        for (String s : titles) {
            if (this.title.length() > 0) this.title += ", ";
            this.title += s;
        }
    }

    @Nullable
    public static synchronized MemberEntityLite Create(String id, MemberEntity source) {
        String name = (String) source.get(MemberEntity.NAME_KEY);
        if (name == null) return null;
        ArrayList<String> titles = (ArrayList<String>) source.get(MemberEntity.TITLES_KEY);
        if (titles == null) return null;
        return new MemberEntityLite(id, name, titles);
    }
}
