package homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Activitys.DetailActivity;
import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Adapters.MembersAdapter;
import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Entities.MemberEntityLite;
import homework.skillbranch.xinvestoriginal.denisgeikohomework.R;

/**
 * Created by x-inv on 12.10.2016.
 */

public  class HouseFragment extends Fragment implements AdapterView.OnItemClickListener {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private ListView lvHouseMembers;
    private HousePresenter housePresenter;
    public HouseFragment() {
    }

    public static HouseFragment newInstance(int sectionNumber) {
        HouseFragment fragment = new HouseFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_house_members, container, false);
        int framePosition = getArguments().getInt(ARG_SECTION_NUMBER);
        lvHouseMembers = (ListView)rootView.findViewById(R.id.lvHouseMembers);
        lvHouseMembers.setOnItemClickListener(this);
        housePresenter = new HousePresenter(this, framePosition);
        return rootView;
    }

    public void FillList(int iconID, ArrayList<MemberEntityLite> members) {
        lvHouseMembers.setAdapter(new MembersAdapter(getContext(), members, iconID));
    }



    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        MemberEntityLite member = (MemberEntityLite) parent.getAdapter().getItem(position);
        DetailActivity.Show(getActivity(), housePresenter.GetHouseName(), member.id);
    }
}