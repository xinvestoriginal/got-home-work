package homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson2AuthScreen.Models;

import android.content.Context;
import android.os.AsyncTask;

import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson2AuthScreen.Presenters.RootPresenter;

/**
 * Created by x-inv on 22.10.2016.
 */

public class AuthHelper {

    private static AuthHelper instance = null;
    public static synchronized AuthHelper GetInstanse(Context context){
        if (instance == null){
            instance = new AuthHelper(context);
        }
        return instance;
    }

    private Context context;
    public RootPresenter rootPresenter;

    private AuthHelper(Context context){
        this.context = context;
        this.rootPresenter = null;
    }


    public boolean isAuth(){
        return false;
    }

    public void GetToken(String mail, String pass){
        new AsyncTask<Object,Object,Object>(){

            @Override
            protected Object doInBackground(Object... params) {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object result) {
                super.onPostExecute(result);
                String token = (String) result;
                SetToken(token);
                final RootPresenter p = rootPresenter;
                if (p != null) rootPresenter.onTokenObtained();
            }

        }.execute();
    }

    private void SetToken(String token){

    }


}
