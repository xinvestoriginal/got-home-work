package homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.SQLite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Entities.HouseEntity;
import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Entities.MemberEntity;
import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Entities.MemberEntityLite;

/**
 * Created by x-inv on 13.10.2016.
 */

public class BaseHelper extends DefaultBaseHelper {

    public static final String MEMBER_TABLE = "members";
    public static final String HOUSE_TABLE = "houses";
    private static final String DATA_KEY = "data";
    private static final String HOUSE_KEY = "house";
    private static final String URL_KEY = "url";
    private static final String BASE_NAME = "GOT_BASE";
    private static BaseHelper instance = null;
    //--------------------------------------------------------------------
    public BaseHelper(Context context) {
        super(context,BASE_NAME);
    }

    public BaseHelper(Context context, String baseName) {
        super(context, baseName);
    }

    public  static void Init(Context context){ GetInstance(context); }

    public  static synchronized BaseHelper GetInstance() {
        return GetInstance(null);
    }
    //--------------------------------------------------------------------

    private static synchronized BaseHelper GetInstance(Context context) {
        if (instance == null) instance = new BaseHelper(context);
        return instance;
    }

    @Override
    protected String[] GetDefaultTypes(String[] columns) {
        String[] res = new String[columns.length];
        for (int i = 0; i < res.length; i++){
            res[i] = "text";
        }
        return res;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(GetCreateTableString(HOUSE_TABLE, new String[]{HOUSE_KEY, DATA_KEY}));
        db.execSQL(GetCreateTableString(MEMBER_TABLE, new String[]{HOUSE_KEY, DATA_KEY, URL_KEY}));
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void AddHouseAsString(String house, String data) {
        Insert(HOUSE_TABLE, new String[]{HOUSE_KEY, DATA_KEY}, new String[]{house, data});
    }

    public void AddMemberAsString(String house, String data, String url) {
        Insert(MEMBER_TABLE, new String[]{HOUSE_KEY, DATA_KEY, URL_KEY}, new String[]{house, data, url});
    }

    public MemberEntity GetMember(String id) {
        ArrayList<String[]> mRows = Select(MEMBER_TABLE, ID, id);
        if (mRows.size() == 0) return null;
        String jsonStr = mRows.get(0)[2];
        try {
            return new MemberEntity(new JSONObject(jsonStr));
        } catch (JSONException e) {
            return null;
        }
    }

    @Nullable
    public String GetMemberIdFromUrl(String url) {
        if (url == null || url.length() == 0) return null;
        ArrayList<String[]> mRows = Select(MEMBER_TABLE, URL_KEY, url);
        return mRows.size() == 0 ? null : mRows.get(0)[0];
    }

    public ArrayList<MemberEntityLite> GetMembers(String house) {
        ArrayList<String[]> mRows = Select(MEMBER_TABLE,HOUSE_KEY,house);
        Collections.sort(mRows, new Comparator<String[]>() {
            @Override
            public int compare(String[] o1, String[] o2) {
                int idA = Integer.parseInt(o1[0]);
                int idB = Integer.parseInt(o2[0]);

                return idA == idB ? 0 : idA < idB ? -1 : 1;
            }
        });
        ArrayList<MemberEntityLite> res = new ArrayList<>();
        for (String[] mRow : mRows){
            String data = mRow[2];
            try {
                String id = mRow[0];
                MemberEntity mItem = new MemberEntity(new JSONObject(data));
                MemberEntityLite member = MemberEntityLite.Create(id, mItem);
                if (member != null) res.add(member);
            } catch (JSONException e) {

            }
        }
        return res;
    }

    public HouseEntity LoadHouse(String name) {
        //Log.e("base",String.valueOf(Select(HOUSE_TABLE).size()));
        ArrayList<String[]> hRows = Select(HOUSE_TABLE, HOUSE_KEY, name);
        //Log.e("base",name + "  " + String.valueOf(hRows.size()));
        if (hRows.size() == 0) return null;
        String data = hRows.get(0)[2];
        HouseEntity res;
        try {

            HouseEntity hItem = new HouseEntity(new JSONObject(data));
            if (hItem.noError) {
                hItem.members = null; //GetMembers(hItem.GetName());
                res = hItem;
            } else {
                res = null;
            }
        } catch (JSONException e) {
            res = null;
        }

        return res;
    }

    public int GetHouseCount() {
        return GetCount(HOUSE_TABLE);
    }

    public ArrayList<HouseEntity> LoadHouses() {
        ArrayList<String[]> hRows = Select(HOUSE_TABLE);
        Collections.sort(hRows, new Comparator<String[]>() {
            @Override
            public int compare(String[] o1, String[] o2) {
                int idA = Integer.parseInt(o1[0]);
                int idB = Integer.parseInt(o2[0]);

                return idA == idB ? 0 : idA < idB ? -1 : 1;
            }
        });
        ArrayList<HouseEntity> res = new ArrayList<>();
        for (String[] hRow : hRows) {
            String data = hRow[2];
            try {
                //Log.e("base",data);
                HouseEntity hItem = new HouseEntity(new JSONObject(data));
                if (hItem.noError) {
                    hItem.members = null; //GetMembers(hItem.GetName());
                    res.add(hItem);
                }
            } catch (JSONException e) {

            }
        }
        return res;
    }
}
