package homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson2AuthScreen.Activityes;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson2AuthScreen.Helpers.TypefaceHelper;
import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson2AuthScreen.Models.AuthState;
import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson2AuthScreen.Presenters.RootPresenter;
import homework.skillbranch.xinvestoriginal.denisgeikohomework.R;

public class RootActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String STATE_KEY = "state";
    private AuthState state;
    private RootPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);
        presenter = new RootPresenter(this);
        state = AuthState.Create(this,null);

        TextView tvAuthAppName = (TextView)findViewById(R.id.tvAuthAppName);
        TypefaceHelper.SetTypeface(this,tvAuthAppName);

        findViewById(R.id.bAuthGo).setOnClickListener(this);
        findViewById(R.id.bAuthShowCatalog).setOnClickListener(this);
        findViewById(R.id.ivAuthGoFB).setOnClickListener(this);
        findViewById(R.id.ivAuthGoTV).setOnClickListener(this);
        findViewById(R.id.ivAuthGoVC).setOnClickListener(this);
    }

    public void showLoad(){
        state.SetLoadState(true);
    }

    public void hideLoad(){
        state.SetLoadState(false);
    }

    public void ShowPanel(){
        state.SetPanelVisible(true);
    }

    public void HidePanel(){
        state.SetPanelVisible(false);
    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString(STATE_KEY,state.toJStr());
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        state = AuthState.Create(this,savedInstanceState.getString(STATE_KEY,null));
    }

    @Override
    public void onBackPressed(){
        if (state.IsLoad()) return;
        if (state.PanelIsOpen()) state.SetPanelVisible(false); else finish();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bAuthGo:
                if (state.PanelIsOpen()){
                    String vEmail = ((EditText)findViewById(R.id.etAuthMail)).getText().toString();
                    String vPass = ((EditText)findViewById(R.id.etAuthPassword)).getText().toString();
                    presenter.GetToken(vEmail,vPass);
                }else{
                    ShowPanel();
                }
                break;
            case R.id.ivAuthGoFB:
                presenter.clickOnFb();
                break;
            case R.id.ivAuthGoTV:
                presenter.clickOnTwitter();
                break;
            case R.id.ivAuthGoVC:
                presenter.clickOnVk();
                break;
            case R.id.bAuthShowCatalog:
                presenter.clickOnShowCatalog();
                break;

        }
    }
}
