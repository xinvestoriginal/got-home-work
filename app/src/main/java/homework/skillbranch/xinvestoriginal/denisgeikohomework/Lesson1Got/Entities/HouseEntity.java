package homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson1Got.Entities;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by x-inv on 12.10.2016.
 */

public class HouseEntity extends ProtoEntity{

    public  static final String NAME = "name";
    private static final String SWORN_MEMBERS = "swornMembers";

    private static final String[] strKeys = {"url", NAME, "region", "coatOfArms",
            "words", "currentLord",  "heir", "overlord", "founded", "founder", "diedOut" };


    private static final String[] strArrs = {"titles","seats","ancestralWeapons",
                                             "cadetBranches", SWORN_MEMBERS};
    public ArrayList<MemberEntity> members = null;

    public HouseEntity(JSONObject source){
        Fill(source);
    }

    public String GetName(){
        return (String)this.get(NAME);
    }

    /*
    public String GetMemberNameFromUrl(String url){
        MemberEntity e = GetMemberFromUrl(url);
        return e == null ? "" : (String) e.get(MemberEntity.NAME_KEY);
    }

    public MemberEntity GetMemberFromUrl(String url){
        MemberEntity res = null;
        for (int i = 0; res == null && i < members.size();i++){
            MemberEntity memberEntity = members.get(i);
            if (url.equals(memberEntity.get(MemberEntity.URL_KEY))){
                res = memberEntity;
            }
        }
        return res;
    }
*/
    public ArrayList<String> GetMembersUrls(){
        return (ArrayList<String>)this.get(SWORN_MEMBERS);
    }

    @Override
    protected String[] StrKeys() {
        return strKeys;
    }

    @Override
    protected String[] ArrKeys() {
        return strArrs;
    }



}
