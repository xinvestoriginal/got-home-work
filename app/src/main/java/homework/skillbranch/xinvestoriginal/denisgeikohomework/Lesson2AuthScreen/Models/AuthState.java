package homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson2AuthScreen.Models;

import android.view.View;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import homework.skillbranch.xinvestoriginal.denisgeikohomework.Lesson2AuthScreen.Activityes.RootActivity;
import homework.skillbranch.xinvestoriginal.denisgeikohomework.R;

/**
 * Created by x-inv on 22.10.2016.
 */

public class AuthState extends JSONObject {

    private RootActivity activity;

    private static final String email       = "email";
    private static final String pass        = "pass";
    private static final String panelIsOpen = "panelIsOpen";
    private static final String onLoad      = "onLoad";



    public static AuthState Create(RootActivity activity, String json){
        AuthState res;
        try {
            res = json == null ?  new AuthState() : new AuthState(json);
        } catch (JSONException e) {
            res = new AuthState();
        }
        res.activity = activity;
        try {
            res.Init();
        } catch (JSONException e) {

        }
        return res;
    }

    public String toJStr(){
        try {
            String vEmail = ((EditText)activity.findViewById(R.id.etAuthMail)).getText().toString();
            put(email,vEmail);
            String vPass = ((EditText)activity.findViewById(R.id.etAuthPassword)).getText().toString();
            put(pass,vPass);
        } catch (JSONException e) {

        }
        return this.toString();
    }

    public void SetPanelVisible(boolean state){
        View llAuthPanelContainer = activity.findViewById(R.id.llAuthPanelContainer);
        View llAuthShowButtonContainer = activity.findViewById(R.id.llAuthShowButtonContainer);
        try {
            if (state){
                llAuthPanelContainer.setVisibility(View.VISIBLE);
                llAuthShowButtonContainer.setVisibility(View.GONE);
            }else{
                llAuthPanelContainer.setVisibility(View.GONE);
                llAuthShowButtonContainer.setVisibility(View.VISIBLE);
            }
            put(panelIsOpen,state);
        } catch (JSONException e) {

        }
    }

    public boolean IsLoad(){
        try {
            return getBoolean(onLoad);
        } catch (JSONException e) {
            return false;
        }
    }

    public boolean PanelIsOpen(){
        try {
            return getBoolean(panelIsOpen);
        } catch (JSONException e) {
            return false;
        }
    }

    public void SetLoadState(boolean state){
        View llAuthMainContainer = activity.findViewById(R.id.llAuthMainContainer);
        View pbAuthLoad = activity.findViewById(R.id.pbAuthLoad);
        try {
            if (state){
                llAuthMainContainer.setVisibility(View.GONE);
                pbAuthLoad.setVisibility(View.VISIBLE);
            }else{
                llAuthMainContainer.setVisibility(View.VISIBLE);
                pbAuthLoad.setVisibility(View.GONE);
            }
            put(onLoad,state);
        } catch (JSONException e) {

        }
    }



    private AuthState(String json) throws JSONException {
        super(json);
    }

    private AuthState() {
        super();
    }

    private void Init() throws JSONException {
        String vEmail = has(email) ? getString(email) : "";
        ((EditText)activity.findViewById(R.id.etAuthMail)).setText(vEmail);

        String vPass = has(pass) ? getString(pass) : "";
        ((EditText)activity.findViewById(R.id.etAuthPassword)).setText(vPass);

        boolean vOnLoad = has(onLoad) ? getBoolean(onLoad) : false;
        if (vOnLoad){
            SetLoadState(vOnLoad);
        }else{
            boolean vPanelIsOpen = has(panelIsOpen) ? getBoolean(panelIsOpen) : false;
            SetPanelVisible(vPanelIsOpen);
        }
    }

}
